# Python Projects

Apps from [this](https://www.udemy.com/course/the-python-pro-course) Udemy course. Original
source codes are in [this](https://github.com/pythonprocourse/files) repository.

### APP 1 - Get coordinates antipode - [Original Project](https://github.com/pythonprocourse/app1)
### APP 2 - Flatmates' Bills - [Original Project](https://github.com/pythonprocourse/app2)
### App 3 - Project-Math-Painting - [Original Project](https://github.com/pythonprocourse/app3)
### App 4 - Webcam-Photo-Sharer - [Original Project](https://github.com/pythonprocourse/app4)
### App 5 - Flatmates-Bill-Web-App - [Original Project](https://github.com/pythonprocourse/app5)
### App 6 - Project-Calorie-Webapp - [Original Project](https://github.com/pythonprocourse/app6)
### App 7 - Automated-Emails - [Original Project](https://github.com/pythonprocourse/app7)
### App 8 - Instant-Dictionary-Webapp - [Original Project](https://github.com/pythonprocourse/app8)
### App 9 - Instant-Dictionary-API - [Original Project](https://github.com/pythonprocourse/app9)
### App 10 - Cinema-Ticket-Booking - [Original Project](https://github.com/pythonprocourse/app10)
