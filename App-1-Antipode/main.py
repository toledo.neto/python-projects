from folium import Map, Popup
from geo import Geopoint

latitude = 40.4
longitude = 3.7

antipode_latitude = latitude*-1

if longitude <= 0:
    antipode_longitude = longitude + 180
elif longitude == 0:
    antipode_longitude = 180
else:
    antipode_longitude = longitude - 180

mymap = Map(location=[antipode_latitude, antipode_longitude])

geopoint = Geopoint(antipode_latitude, antipode_longitude)
popup = Popup(str(geopoint.get_weather()))
popup.add_to(geopoint)
geopoint.add_to(mymap)

mymap.save("antipode.html")

print(antipode_longitude)
print(antipode_latitude)
print(geopoint.closest_parallel())
