from app6.settings import BASE_DIR

from selectorlib import Extractor
import requests


class Temperature:

    def __init__(self, country, city):
        self.city = city.replace(" ", "-")
        self.country = country.replace(" ", "-")

    def get(self):
        req = requests.get(f'https://www.timeanddate.com/weather/{self.country}/{self.city}')
        html = req.text

        extractor = Extractor.from_yaml_file(str(BASE_DIR)+'/temperature.yaml')
        raw_result = extractor.extract(html=html)

        # salvar em texto
        return float(raw_result['temp'].replace('\xa0°C', '').strip())


if __name__ == "__main__":
    temp = Temperature('brazil', 'brasilia').get()
    print(temp)
