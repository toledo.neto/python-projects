from classes.temperature import Temperature


class CalorieCalculator:

    def __init__(self, weight, height, age, temperature):
        self.temperature = temperature
        self.age = age
        self.height = height
        self.weight = weight

    def calculate(self):
        # BMR formula
        return 10 * self.weight + 6.25 * self.height - 5 * self.age + 5 \
               - 10 * self.temperature


if __name__ == "__main__":
    temp = Temperature('brazil', 'anapolis').get()
    print(CalorieCalculator(100, 1.75, 31, temp).calculate())
