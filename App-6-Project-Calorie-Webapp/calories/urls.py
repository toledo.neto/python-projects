from . import views
from django.urls import path


app_name = 'calories'
urlpatterns = [
    path('', views.CaloriesFormView.as_view(), name='calories_form_page'),
]
