from django import forms


class ContactForm(forms.Form):
    weight = forms.FloatField()
    height = forms.FloatField()
    age = forms.IntegerField()
    city = forms.CharField()
    country = forms.CharField()
