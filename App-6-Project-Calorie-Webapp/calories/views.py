from django.shortcuts import render
from django.views.generic.edit import FormView

from classes.temperature import Temperature


from .forms import ContactForm


class CaloriesFormView(FormView):
    template_name = 'calories_form_page.html'
    form_class = ContactForm
    success_url = 'calories:calories_form_page'

    def calculateBMR(self, data):
        temperature = Temperature(data['country'], data['city']).get()
        return 10 * float(data['weight']) + 6.25 * float(data['height']) \
               - 5 * float(data['age']) + 5 - 10 * temperature

    def post(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context['result'] = self.calculateBMR(request.POST)
        return render(request, self.template_name, context)
